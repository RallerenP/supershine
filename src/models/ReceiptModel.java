package models;

import app.Receipt;

public class ReceiptModel extends Model {
    public ReceiptModel() {
        preText = "Would you like a receipt?\n";
        options.add("Yes");
        options.add("No");
        postText = "Please choose an option: ";
    }

    public Model input(String input) {
        if (input.equals("1")) {
            Receipt receipt = Receipt.getInstance();
            receipt.print();
            return new EndModel();
        } else if (input.equals("2")) {
            return new EndModel();
        } else {
            return new ReceiptModel();
        }

    }
}
