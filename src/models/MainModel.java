package models;

import app.*;

public class MainModel extends Model {
    MainModel() {
        this("Welcome " + new ColoredString(WashCard.getInstance().getName(), Color.RED, true).toString() +"!\n" +
                "You have " + new ColoredString(WashCard.getInstance().getBalance() + "kr", Color.YELLOW, true) + " on your account!\n");
    }

    private MainModel(String preText) {
        ProductCatalog pc = ProductCatalog.getInstance();
        double cheapest = pc.getWashTypes().get(0).getPrice();

        WashCard wc = WashCard.getInstance();
        double balance = wc.getBalance();

        this.preText = preText;
        options.add((cheapest >= balance) ? new ColoredString("Wash Car", Color.WHITE).toString() : "Wash Car" );
        options.add("Recharge WashCard");
        postText = "Please choose an option (q for quit at any time): ";

    }

    public Model input(String input) {
        if (checkQuit(input)) {
            return end();
        }

        if (isInvalidOption(input)) {
            return new MainModel("Error! Please choose option 1, 2 or 3\n");
        }

        if (input.equals("1")) {
            return new WashModel();
        }

        if (input.equals("2")) {
            return new RechargeModel(0);
        }

        return new Model();
    }
}

