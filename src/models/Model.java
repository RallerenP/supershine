package models;

import app.Receipt;

import java.util.ArrayList;

public class Model {
    String preText = "This is a template model: ";
    boolean end = false;
    boolean skipInput = false;
    ArrayList<String> options = new ArrayList<>();
    String postText = "";

    Model() {

    }

    //region Getters
    public String getPreText() {
        return preText;
    }

    public ArrayList<String> getOptions() {
        return options;
    }

    public String getPostText() {
        return postText;
    }

    public boolean getEnd() {
        return end;
    }

    public boolean getSkipInput() {
        return skipInput;
    }

    //endregion

    public Model input(String input) {
        return new EndModel();
    }

    boolean checkQuit(String input) {
        return input.equals("q");
    }

    Model end() {
        if (Receipt.getInstance().getLines().size() > 0)
                return new ReceiptModel();
            else
                return new EndModel();
    }

    boolean isInvalidOption(int option) {
        return option <= options.size() && option >= 1;
    }

    boolean isInvalidOption(String option) {
        try {
            return !isInvalidOption(Integer.parseInt(option));
        } catch (NumberFormatException e) {
            return true;
        }
    }

    public Model skipped() {
        return new Model();
    }


}
