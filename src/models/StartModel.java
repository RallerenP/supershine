package models;

import app.WashCard;

public class StartModel extends Model {
    public StartModel() {
        preText = (char)27 + "[30;1m" + "\nPlease insert WashCard: ";
    }

    public Model input(String input) {
        WashCard card = WashCard.getInstance();

        if (!card.read(input)) {
            return new ErrorModel("Couldn't read card, contact staff", new StartModel());
        }

        if (card.verify().equals("INVALID_CARD")) {
            return new ErrorModel("Your card is not valid, contact staff", new StartModel());
        }

        if (card.verify().equals("FILE_NOT_FOUND")) {
            return new ErrorModel("FATAL ERROR, contact staff", new EndModel());
        }

        if (card.isOwner().equals("IS_OWNER")) {
            return new OwnerModel();
        } else if (card.isOwner().equals("NOT_OWNER")) {
            return new MainModel();
        } else if (card.isOwner().equals("FILE_NOT_FOUND")){
            return new ErrorModel("FATAL ERROR, contact staff", new EndModel());
        }

        return new Model();
    }
}
