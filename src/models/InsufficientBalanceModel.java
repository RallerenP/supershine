package models;

public class InsufficientBalanceModel extends Model {
    private boolean quit;

    InsufficientBalanceModel() {
        this(false);
    }

    InsufficientBalanceModel(boolean quit) {
        preText = "You have insufficient credit on your WashCard!\nDo you wish to recharge?\n";
        options.add("Yes");
        options.add("No");
        postText = "Please select an option: ";
        this.quit = quit;
    }

    public Model input(String input) {
        if (checkQuit(input)) {
            return end();
        }

        if (input.equals("1"))
            return new RechargeModel(0);
        else if (input.equals("2")) {
            if (quit) return new EndModel();
            else return new WashModel();
        }
        else
            return new InsufficientBalanceModel();
    }
}
