package models;

import app.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class WashModel extends Model {
    private ProductCatalog pc = ProductCatalog.getInstance();
    private ArrayList<Wash> washes = pc.getWashTypes();

    WashModel() {
        this(new ColoredString("Please choose a wash type\n"));
    }

    WashModel(ColoredString preText) {
        this.preText = preText.toString();
        WashCard wc = WashCard.getInstance();

        if (wc.getBalance() < washes.get(0).getPrice()) skipInput = true;

        for (Wash wash : washes) {

            boolean canPurchase = wc.getBalance() >= wash.getPrice();
            ColoredString option = new ColoredString(wash.getName() + " - " + wash.getPrice() + "kr", canPurchase ? Color.RESET : Color.WHITE, true);
            ColoredString discount = new ColoredString(" - Early Bird Discount! (-20%)", Color.YELLOW, canPurchase);

            options.add(option.toString() + (wash.isEarlyBird() ? discount : ""));
        }

        postText = "Please enter a number, to choose a wash: ";
    }

    public Model input(String input) {
        if (checkQuit(input)) {
            return end();
        }

        if (isInvalidOption(input)) return new WashModel(new ColoredString("Invalid option!\n", Color.RED));

        Wash selectedWash = washes.get(Integer.parseInt(input) - 1);
        WashCard wc = WashCard.getInstance();

        if (wc.getBalance() < selectedWash.getPrice()) return new InsufficientBalanceModel();

        wc.deductBalance(selectedWash.getPrice());
        Receipt receipt = Receipt.getInstance();
        receipt.addLine(selectedWash.getName(), "-" + selectedWash.getPrice());

        Statistics st = Statistics.getInstance();

        String date = LocalDateTime.now().toString();
        String statistic = date + "$";

        statistic += wc.getID() + "$";
        statistic += "WASH$";
        statistic += selectedWash.getName() + "$";
        statistic += selectedWash.getPrice() + "$";
        statistic += selectedWash.isEarlyBird();

        st.addStatistic(statistic);
        return new ReceiptModel();
    }

    public Model skipped() {
        return new InsufficientBalanceModel(true);
    }
}
