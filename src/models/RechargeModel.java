package models;

import app.*;

import java.time.LocalDateTime;

public class RechargeModel extends Model{
    private final int INSERT_CARD = 0;
    private final int ENTER_PIN = 1;
    private final int ENTER_AMOUNT = 2;

    private int state = -1;

    public RechargeModel(int state) {
        if (state == INSERT_CARD) state0();
        if (state == ENTER_PIN) state1();
        if (state == ENTER_AMOUNT) state2();
    }

    public RechargeModel(String preText, int state) {
        if (state == ENTER_AMOUNT) {
            this.preText = preText;
            this.state = state;
        }
    }

    private void state0() {
        preText = "Please insert your credit card!\n";
        state = 0;
    }

    private void state1() {
        preText = "Please enter a PIN number: ";
        state = 1;
    }

    private void state2() {
        preText = "Please enter amount (min. 50, max. 1000kr): ";
        state = 2;
    }

    public Model input(String input) {
        if (checkQuit(input)) {
            return end();
        }

        CreditCard cc = CreditCard.getInstance();
        if (state == INSERT_CARD) {
            return insertCard(cc);
        }

        if (state == ENTER_PIN) {
            return enterPin(cc, input);
        }

        if (state == ENTER_AMOUNT) {
            return enterAmount(cc, input);
        }
        return new Model();
    }

    private Model enterAmount(CreditCard cc, String number) {
        double amount;
        try {
            amount = Double.parseDouble(number);
        } catch (NumberFormatException e) {
            return new ErrorModel("Not a number!", new RechargeModel(ENTER_AMOUNT));
        }

        if (!(amount >= 50 && amount <= 1000)) return new RechargeModel(new ColoredString("Invalid Amount! (min. 50, max. 1000): ", Color.RED).toString(),2);

        if (cc.deductPaymentSuccess(number)) {
            WashCard wc = WashCard.getInstance();
            wc.addPayment(amount);
            Receipt rc = Receipt.getInstance();
            rc.addLine("Refill", "+" + number);

            Statistics st = Statistics.getInstance();

            String date = LocalDateTime.now().toString();
            String statistic = date + "$";
            statistic += wc.getID() + "$";
            statistic += "RECHARGE$";
            statistic += amount;

            st.addStatistic(statistic);

            return new MainModel();
        } else {
            return new ErrorModel("Card Declined", new MainModel());
        }
    }

    private Model enterPin(CreditCard cc, String PIN) {
        if (cc.verifyPin(PIN)) {
            return new RechargeModel(ENTER_AMOUNT);
        } else {
            return new ErrorModel("PIN declined", new MainModel());
        }
    }

    private Model insertCard(CreditCard cc) {
        if (cc.read() && cc.verify()) {
            return new RechargeModel(ENTER_PIN);
        } else {
            return new ErrorModel("Couldn't read card", new MainModel());
        }
    }
 }
