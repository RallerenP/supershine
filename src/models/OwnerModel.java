package models;

import app.Statistics;

public class OwnerModel extends Model {
    OwnerModel() {
        this.preText = "Hello\n";
        options.add("Print Statistics");
        this.postText = "Thanks for being a Super Shiney! (q for quit): ";
    }

    public Model input(String input) {
        if (checkQuit(input)) {
            return end();
        }

        if (input.equals("1")) {
            if (!Statistics.getInstance().readStatistics()) return new ErrorModel("Statistics 404 not found", new EndModel());
            Statistics.getInstance().printBasic();
            return new EndModel();
        } else {
            return new OwnerModel();
        }
    }
}
