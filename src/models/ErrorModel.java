package models;

import app.Color;
import app.ColoredString;

public class ErrorModel extends Model {
    private Model returnModel;


    ErrorModel(String error, Model returnModel) {
        preText = new ColoredString(error, Color.RED).toString();
        this.returnModel = returnModel;
        skipInput = true;

    }

    public Model skipped() {
        try {
            Thread.sleep(2000);
            return returnModel;
        }
        catch (InterruptedException e){
            return new EndModel();
        }
    }
}
