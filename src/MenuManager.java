import models.Model;
import models.StartModel;


import java.util.ArrayList;
import java.util.Scanner;

class MenuManager {

    private Scanner sc = new Scanner(System.in);
    private Model current = new StartModel();

    public void start() throws InterruptedException {
        while(true) {
            clear();
            display();
            if (current.getEnd()) {
                break;
            }

            if (current.getSkipInput()) {
                current = current.skipped();
            } else {
                current = current.input(sc.nextLine());
            }

        }

        Thread.sleep(2000);
    }

    private void display() {

        String preText = current.getPreText();
        ArrayList<String> options = current.getOptions();
        String postText = current.getPostText();

        System.out.print(preText);

        for (int i = 1; i <= options.size(); i++) {
            System.out.print("\t " + i + ". " + options.get(i - 1) + "\n");
        }

        System.out.print(postText);
    }

    private static void clear() {
        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }
}




