package app;

public class ColoredString {
    private final String value;
    private final Color color;
    private final boolean bright;

    public ColoredString(String value) {
        this(value, Color.RESET, true);
    }

    public ColoredString(String value, Color color) {
        this(value, color, true);
    }

    public ColoredString(String value, Color color, boolean bright) {
        this.value = value;
        this.color = color;
        this.bright = bright;
    }

    @Override
    public String toString() {
        return (char)27 + Color.getCode(color, bright) + value + (char)27 + Color.getCode(Color.RESET, false);
    }
}
