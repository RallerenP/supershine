package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ProductCatalog {
    private ArrayList<Wash> washTypes = new ArrayList<>();

    private static ProductCatalog instance = new ProductCatalog();
    public static ProductCatalog getInstance() { return instance; }

    private ProductCatalog() { }

    private void fetchWashTypes() {
        try {
            Scanner sc = new Scanner(new File("Washtypes"));
            while (sc.hasNextLine()) {
                Wash wash = new Wash();
                String[] washDetails = sc.nextLine().split("\\$");
                String[] instructions = washDetails[2].split("-");
                wash.setName(washDetails[0]);
                wash.setPrice(Double.parseDouble(washDetails[1]));
                wash.setInstructions(instructions);
                wash.setEarlyBird(Boolean.parseBoolean(washDetails[3]));
                washTypes.add(wash);
            }

        }  catch (FileNotFoundException ignored) {

        }
    }

    public ArrayList<Wash> getWashTypes() {
        if (washTypes.size() == 0) {
            fetchWashTypes();
        }

        return washTypes;
    }
}
