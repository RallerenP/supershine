package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

public class Statistics {
    private static Statistics instance = new Statistics();
    public static Statistics getInstance() { return instance; }

    private ArrayList<String[]> statistics = new ArrayList<>();

    private int totalWashes = 0,
            totalEconomy = 0,
            totalStandard = 0,
            totalDeLuxe = 0,
            totalEarlyBirds = 0,
            totalRecharges = 0;

    private double totalWashProfits = 0,
            totalRecharged = 0;

    public boolean readStatistics() {
        try {
            String fileName = "Statistics";
            Scanner sc = new Scanner(new File(fileName));

            while(sc.hasNextLine()) {
                statistics.add(sc.nextLine().split("\\$"));
            }

            return true;

        } catch (FileNotFoundException e) {
            return false;
        }
    }

    public void addStatistic(String statistic) {
        try {
            String fileName = "Statistics";
            PrintStream ps = new PrintStream(new FileOutputStream(fileName, true));
            ps.append(statistic + "\n");
        } catch (FileNotFoundException e) {
            // Do nothing
        }

    }

    public void printBasic() {
        calcStats();
        String fileName = "PrintedStats";

        try {
            PrintStream ps = new PrintStream(new File(fileName));

            ps.println("Statistics since " + LocalDate.now().minusMonths(1));
            ps.println();
            ps.println("Wash Statistics:");
            ps.printf("\tTotal Washes: %d\n\n" +
                    "\tTotal Economy: %d\n" +
                    "\tTotal Standard: %d\n" +
                    "\tTotal De Luxe: %d\n\n" +
                    "\tTotal EarlyBird Uses: %d\n\n" +
                    "\tTotal wash profits: %.2fkr\n\n",
                    totalWashes, totalEconomy, totalStandard, totalDeLuxe, totalEarlyBirds, totalWashProfits);

            ps.println("Recharge Statistics:");
            ps.printf("\tTotal recharges: %d\n\n" +
                    "\tTotal recharged: %.2fkr\n",
                    totalRecharges, totalRecharged);


        } catch (FileNotFoundException ignored) {

        }
    }

    private void calcStats() {
        ArrayList<String[]> tmp = new ArrayList<>();

        for (String[] stat : statistics) {
            LocalDateTime date = LocalDateTime.parse(stat[0]);
            LocalDateTime cutoff = LocalDateTime.now().minusMonths(1);

            if (date.isAfter(cutoff)) {
                tmp.add(stat);
            }
        }



        for (String[] stat : tmp) {
            if (stat[2].equals("WASH")) {
                totalWashes++;

                if(stat[3].equals("Economy")) totalEconomy++;
                if(stat[3].equals("Standard")) totalStandard++;
                if(stat[3].equals("De Luxe")) totalDeLuxe++;

                totalWashProfits += Double.parseDouble(stat[4]);

                if (stat[5].equals("true")) totalEarlyBirds++;
            }

            if (stat[2].equals("RECHARGE")) {
                totalRecharged += Double.parseDouble(stat[3]);
                totalRecharges++;
            }
        }
    }
}
