package app;

public enum Color {
    BLACK,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA,
    CYAN,
    WHITE,
    RESET;

    public static String getCode(Color color, boolean bright)  {
        String code = "";
        switch (color) {
            case BLACK:
            case RESET:
                code += "[30";
                break;
            case RED:
                code += "[31";
                break;
            case GREEN:
                code += "[32";
                break;
            case YELLOW:
                code += "[33";
                break;
            case BLUE:
                code += "[34";
                break;
            case MAGENTA:
                code += "[35";
                break;
            case CYAN:
                code += "[36";
                break;
            case WHITE:
                code += "[37";
                break;
        }

        if (bright) code += ";1";
        code += "m";
        return code;
    }
}
