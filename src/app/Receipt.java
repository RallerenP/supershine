package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

public class Receipt {

    private ArrayList<String> lines = new ArrayList<>();

    private static Receipt instance = new Receipt();
    public static Receipt getInstance() { return instance; }

    private Receipt() {}

    public void addLine(String item, String price) {
        String line = item + createPadding(item.length() + price.length(), 20) + price;
        lines.add(line);
    }
    public ArrayList<String> getLines() {
        return lines;
    }

    public void print() {
        finishReceipt();
        try {
            PrintStream ps = new PrintStream(new File("receipt.txt"));
            for (String line : lines) {
                ps.println(line);
            }
            ps.close();
        } catch (FileNotFoundException e) {
            // Do nothing
        }
    }

    private void finishReceipt() {
        WashCard wc = WashCard.getInstance();
        String balance = String.valueOf(wc.getBalance());
        String oldBalance = String.valueOf(wc.getOriginalBalance());
        lines.add("");
        lines.add("--------------------");
        lines.add("");
        lines.add("Old Balance:" + createPadding(12 + oldBalance.length(), 20) + oldBalance);
        lines.add("New balance:" + createPadding(12 + balance.length(), 20) + balance);
        lines.add("");
        lines.add(" Have a shiney day! ");
    }

    private String createPadding(int currentLength, int maxLength) {
        int padding = maxLength - currentLength;
        String line = "";
        for (int i = 0; i < padding; i++) {
            line += " ";
        }
        return line;
    }
}
