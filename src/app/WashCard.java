package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class WashCard {
    private String ID;
    private String name;
    private double balance;
    private double originalBalance = balance;

    private static WashCard instance = new WashCard();
    public static WashCard getInstance() {
        return instance;
    }

    private WashCard() {

    }

    public boolean read(String input) {
        this.ID = input;
        return true;
    }

    public String verify() {
        try {
            Scanner sc = new Scanner(new File("WashCardList"));
            ArrayList<String[]> washCards = new ArrayList<>();

            while(sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] info = line.split("\\$");
                washCards.add(info);
            }

            for (String[] info : washCards) {
                if (this.ID.equals(info[0])) {
                    this.balance = Double.parseDouble(info[2]);
                    this.name = info[1];
                    this.originalBalance = balance;
                    return "VALID_CARD";
                }
            }
        } catch (FileNotFoundException e) {

            return "FILE_NOT_FOUND";
        }

        return "INVALID_CARD";
    }

    public String isOwner() {
        try {
            Scanner sc = new Scanner(new File("OwnerIDs"));

            while(sc.hasNextLine()) {
                if (sc.nextLine().equals(this.ID)) {
                    return "IS_OWNER";
                }
            }
        } catch (FileNotFoundException e) {

            return "FILE_NOT_FOUND";
        }
        return "NOT_OWNER";
    }

    public String getID() {
        return ID;
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }
    public double getOriginalBalance() {
        return originalBalance;
    }

    public void deductBalance(double amount) {
        balance -= amount;
        save();
    }


    private void save() {
        try {
            File file = new File("WashCardList");
            Scanner sc = new Scanner(file);
            ArrayList<String> lines = new ArrayList<>();

            while(sc.hasNextLine()) {
                String line = sc.nextLine();
                String ID = line.split("\\$")[0];

                if (!ID.equals(this.ID)) lines.add(line);
                else {
                    line = this.ID + "$" + name + "$" + balance;
                    lines.add(line);
                }

            }

            sc.close();

            PrintStream ps = new PrintStream(new File("WashCardList"));

            for(String line : lines) {
                ps.println(line);
            }

        } catch(FileNotFoundException ignored) {

        }
    }


    public void addPayment(double amount) {
        balance += amount;
        save();
    }
}
