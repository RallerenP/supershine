package app;

public class CreditCard {

    private static final CreditCard instance = new CreditCard();
    public static CreditCard getInstance() { return instance; }

    /**
     * Reads the data of the card.
     * @return returns true if data was successfully read.
     */
    public boolean read() {
        return true;
    }

    /**
     * Initiates verification of the card
     * @return returns true if the card was successfully verified
     */
    public boolean verify() {
        return true;
    }

    /**
     * Initiates verification of the entered PIN
     * @param PIN The PIN number entered by the user
     * @return returns true if the PIN number belongs to the card
     */
    public boolean verifyPin(String PIN) {
        return true;
    }

    /**
     * Tries to deduct payment from the customers card
     * @param amount The amount of money to deduct
     * @return Returns true if the payment successfully completed
     */
    public boolean deductPaymentSuccess(String amount) {
        return true;
    }
}
