package app;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

public class Wash {

    private String name;
    private double price;
    private String[] instructions;
    private boolean earlyBird;

    public Wash() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        double discount = 0.2;
        return isEarlyBird() ? price - (price * discount) : price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String[] getInstructions() {
        return instructions;
    }

    public void setInstructions(String[] instructions) {
        this.instructions = instructions;
    }

    public boolean isEarlyBird() {
        if (!earlyBird) return false;

        DayOfWeek day = LocalDateTime.now().getDayOfWeek();
        int hour = LocalDateTime.now().getHour();
        return (hour < 14 && (day == DayOfWeek.MONDAY));
    }

    public void setEarlyBird(boolean earlyBird) {
        this.earlyBird = earlyBird;
    }
}
